## Our very own registry
- write and apply `registry_v01.yml`

## Our very own frontend image
- write and apply `frontend_v8.yml`

## Registry volume for persistence
- write and apply `registry_v02.yml`

## Organized configuration
- write and apply `k8s_v1`
- from now on to apply the config use `kubectl apply -k folder_name`

## PHP Backend setup
- write and apply `k8s_v2`

## Code volume for development
- write and apply `k8s_v3`

## Kustomize code volume for only local
- write and apply `k8s_v4`

## Our first Persistent Volume for live registry storage
- write and apply `k8s_v5`
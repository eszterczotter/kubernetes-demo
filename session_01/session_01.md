* turn on kubernetes in docker
* install [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
* see/set context `cat ~/.kube/config`
* `kubectl cluster-info`
* `kubectl get all`

## Our first pod
* first pod - write `frontend_v1.yml`
* `kubectl apply -f frontend_v1.yml`
* `kubectl get all`
* Cmd + D to split screen to 2 halves (H1 & H2)
* H2 - `kubectl get pod frontend-pod -w`
* H1 - `kubectl describe pod/frontend-pod`
* H1 - `kubectl exec -it pod/frontend-pod -- sh`
* H1 - `curl localhost`
* H1 - `curl [IP_FROM_POD_DESCRIPTION]`
* H1 - `exit`
* H1 - `kubectl delete -f frontend_v1.yml`

## Pod ip is accessable from another pod
* second pod - write `frontend_v2.yml`
* H1 - `kubectl apply -f frontend_v2.yml`
* H1 - `kubectl describe pod/frontend-pod-1`
* H1 - `kubectl exec -it pod/frontend-pod-2 -- sh`
* H1 - `curl [IP_FROM_FIRT_POD_DESCRIPTION]`
* H1 - `exit`
* H1 - `kubectl delete -f frontend_v2.yml`

## Our first service
* first service - write `frontend_v3.yml`
* H1 - `kubectl apply -f frontend_v3.yml`
* H2 - Cmd + Shift + D x2 to split to three halves (H21 & H22 & H23)
* H22 - `kubectl get service -w`
* H23 - Cmd + D to split to to halves (H231 & H232)
* H231 & H232 - `kubectl logs frontend-pod-x -f`
* H1 - `kubectl describe service/frontend-service`
* H1 - `kubectl exec -it service/frontend-service -- sh`
* H1 - `curl 10.100.100.1`
* H1 - `curl frontend-service.default`
* H1 - `exit`
* H1 - `kubectl delete -f frontend_v3.yml`

## Our first node-port
* first node-port - write `frontend_v4.yml`
* H1 - `kubectl apply -f frontend_v4.yml`
* H231 - `exit`
* H23 - `kubectl logs service/frontend-service`
* H1 - `curl localhost:30080`
* H1 - `kubectl delete -f frontend_v4.yml`

## Our first load-balancer
* first load-balancer - write `frontend_v5.yml`
* H1 - `kubectl apply -f frontend_v5.yml`
* H23 - `kubectl logs service/frontend-service`
* H1 - `curl localhost:[NODEPORT_ASSIGNED]` - no longer works
* H1 - `curl localhost:800`
* H1 - `kubectl delete -f frontend_v5.yml`

## Our first deployment
* first deployment - write `frontend_v6.yml`
* H1 - `kubectl apply -f frontend_v6.yml`
* H23 - Cmd + Shift + D to split horizontally
* H23 - `kubectl logs deployment/frontend-deployment`
* H24 - `kubectl get deployment/frontend-deployment -w`
* H1 - `kubectl delete pod [ONE_OF_THE_PODS]` - then watch it get resurrected!
* H1 - `kubectl describe deployment/frontend-deployment`
* H1 - `kubectl get replicaset`
* H1 - `kubectl rollout status deployment/frontend-deployment`
* H1 - `kubectl rollout history deployment/frontend-deployment`

## Our first rollout
* first rollout - write `frontend_v7.yml`
* H1 - `curl localhost:800 -v`
* H1 - `kubectl apply -f frontend_v7.yml`
* H1 - `kubectl rollout status deployment/frontend-deployment`
* H1 - `kubectl rollout pause deployment/frontend-deployment`
* H1 - `kubectl rollout resume deployment/frontend-deployment`
* H1 - `kubectl rollout status deployment/frontend-deployment`
* H1 - `kubectl rollout history deployment/frontend-deployment`
* H1 - `curl localhost:800 -v`
* H1 - `kubectl get replicaset`
